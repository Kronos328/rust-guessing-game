# Number Guessing Game

A small project made to start my learning journey in the Rust language. It can be found on the [second chapter of the "The Rust Programming Language" book.](https://doc.rust-lang.org/stable/book/ch02-00-guessing-game-tutorial.html)

The project consists of a simple game where the computer generates a random number between 0 and 100 and you have to guess what that number is.

# Running

There's not much needed in order to run it. After cloning the repository and ensuring you have Rust installed, enter the root directory of the project and execute the command: `cargo run`.

To exit the program, use the shortcut `Ctrl+C`
